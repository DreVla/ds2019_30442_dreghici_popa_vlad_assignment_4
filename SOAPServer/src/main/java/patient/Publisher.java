package patient;

import javax.xml.ws.Endpoint;

public class Publisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/patients", new PatientServiceImpl());
        Endpoint.publish("http://localhost:8080/activity", new ActivityServiceImpl());
        System.out.println("Server Running ok");
    }
}
