package patient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ActivityRepoImpl implements ActivityRepo{

    private List<Activity> activities;

    public ActivityRepoImpl() {
        activities = new ArrayList<>();
        loadActivities();

    }

    public List<Activity> getAllActivities() {
        return activities;
    }


    private void loadActivities() {
        try {
            Scanner scanner = new Scanner(new File("activity.txt"));
            while (scanner.hasNextLine()) {
                String currentString = scanner.nextLine();
                String startDateString = currentString.substring(0, 19);
                String endDateString = currentString.substring(21, 40);
                String activity = currentString.substring(42);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                Date startDate = null;
                Date endDate = null;
                try {
                    startDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(startDateString);
                    endDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(endDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long durationInMillis = Math.abs(endDate.getTime() - startDate.getTime());
                long durationInMinutes = TimeUnit.MINUTES.convert(durationInMillis,TimeUnit.MILLISECONDS);
                long durationInHours = durationInMinutes/60;
                long leftMinutes = durationInMinutes%60;

                Activity receivedActivity = new Activity();
                receivedActivity.setName(activity);
                receivedActivity.setDate(Integer.parseInt(startDateString.substring(8,10)));
                receivedActivity.setDuration((int)durationInMinutes);
                activities.add(receivedActivity);
                //System.out.println(startDate + " " + endDate + " " + activity + " " + durationInHours + ":" + leftMinutes);
                if(durationInHours >= 12){
                    if(activity.contains("Sleeping") || activity.contains("Leaving")){
                        //System.out.println("Something is wrong with patient. Check Now!");
                    }
                }
                if(durationInHours >=1){
                    if(activity.contains("Toileting") || activity.contains("Showering") || activity.contains("Grooming")){
                        //System.out.println("Something is wrong with patient. Check Now!");
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


//        activities = new ArrayList<>();
//        Activity a1 = new Activity();
//        a1.setDate(28);
//        a1.setName("Sleeping");
//        a1.setDuration(12);
//        activities.add(a1);
//
//        Activity a2 = new Activity();
//        a2.setDate(28);
//        a2.setName("Toileting");
//        a2.setDuration(1);
//        activities.add(a2);
//
//        Activity a3 = new Activity();
//        a3.setDate(28);
//        a3.setName("Showering");
//        a3.setDuration(1);
//        activities.add(a3);
//
//        Activity a4 = new Activity();
//        a4.setDate(28);
//        a4.setName("Sleeping");
//        a4.setDuration(12);
//        activities.add(a4);
//
//        Activity a5 = new Activity();
//        a5.setDate(28);
//        a5.setName("Sleeping");
//        a5.setDuration(12);
//        activities.add(a5);
//
//        Activity a6 = new Activity();
//        a6.setDate(28);
//        a6.setName("Sleeping");
//        a6.setDuration(12);
//        activities.add(a6);
//
//        Activity a7 = new Activity();
//        a7.setDate(28);
//        a7.setName("Sleeping");
//        a7.setDuration(12);
//        activities.add(a7);
//
//        Activity a8 = new Activity();
//        a8.setDate(28);
//        a8.setName("Sleeping");
//        a8.setDuration(12);
//        activities.add(a8);
//
//        Activity a9 = new Activity();
//        a9.setDate(28);
//        a9.setName("Sleeping");
//        a9.setDuration(12);
//        activities.add(a9);
//
//        Activity a10 = new Activity();
//        a10.setDate(28);
//        a10.setName("Sleeping");
//        a10.setDuration(12);
//        activities.add(a10);
    }

}