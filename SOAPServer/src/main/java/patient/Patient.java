package patient;

import java.util.List;

public class Patient {
    private int id;
    private String name;
    private List<Prescription> prescriptionList;

    public Patient() {
    }

    public Patient(int id, String name, List<Prescription> prescriptions) {
        this.id = id;
        this.name = name;
        this.prescriptionList = prescriptions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Prescription> getPrescriptionList() {
        return prescriptionList;
    }

    public void setPrescriptionList(List<Prescription> prescriptionList) {
        this.prescriptionList = prescriptionList;
    }
}
