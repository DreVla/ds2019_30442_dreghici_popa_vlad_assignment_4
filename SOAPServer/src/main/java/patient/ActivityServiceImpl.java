package patient;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ActivityServiceImpl implements ActivityService {

    @Inject
    private ActivityRepoImpl activityRepo;

    public ActivityServiceImpl() {
        this.activityRepo = new ActivityRepoImpl();
    }


    @Override
    public List<Activity> getAllActivities() {
        return activityRepo.getAllActivities();
    }
}