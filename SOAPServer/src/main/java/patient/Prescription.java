package patient;

public class Prescription {
    private int id;
    private String medication;
    private String taken;
    private String intake;

    public Prescription() {
    }

    public Prescription(int id, String medication, String taken, String intake) {
        this.id = id;
        this.medication = medication;
        this.taken = taken;
        this.intake = intake;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }
}

