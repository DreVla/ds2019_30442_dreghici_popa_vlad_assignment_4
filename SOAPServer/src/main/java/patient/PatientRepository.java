package patient;

import java.util.List;

public interface PatientRepository {

    List<Patient> getAllPatients();

    Patient getPatient(int id);
}
