package patient;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface PatientService {

    @WebMethod
    Patient getPatient(int id);

    @WebMethod
    List<Patient> getAllPatients();
}
