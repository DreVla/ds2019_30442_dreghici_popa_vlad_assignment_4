
package patient;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.0
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ActivityServiceImpl", targetNamespace = "http://patient/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ActivityServiceImpl {


    /**
     * 
     * @return
     *     returns java.util.List<patient.Activity>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getAllActivities", targetNamespace = "http://patient/", className = "patient.GetAllActivities")
    @ResponseWrapper(localName = "getAllActivitiesResponse", targetNamespace = "http://patient/", className = "patient.GetAllActivitiesResponse")
    @Action(input = "http://patient/ActivityServiceImpl/getAllActivitiesRequest", output = "http://patient/ActivityServiceImpl/getAllActivitiesResponse")
    public List<Activity> getAllActivities();

}
