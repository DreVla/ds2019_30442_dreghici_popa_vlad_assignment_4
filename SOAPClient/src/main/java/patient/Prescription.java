
package patient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prescription complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prescription"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="intake" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medication" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prescription", propOrder = {
    "id",
    "intake",
    "medication",
    "taken"
})
public class Prescription {

    protected int id;
    protected String intake;
    protected String medication;
    protected String taken;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the intake property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntake() {
        return intake;
    }

    /**
     * Sets the value of the intake property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntake(String value) {
        this.intake = value;
    }

    /**
     * Gets the value of the medication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedication() {
        return medication;
    }

    /**
     * Sets the value of the medication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedication(String value) {
        this.medication = value;
    }

    /**
     * Gets the value of the taken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaken() {
        return taken;
    }

    /**
     * Sets the value of the taken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaken(String value) {
        this.taken = value;
    }

}
