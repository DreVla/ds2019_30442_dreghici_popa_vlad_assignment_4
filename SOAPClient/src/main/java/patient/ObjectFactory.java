
package patient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the patient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAllPatients_QNAME = new QName("http://patient/", "getAllPatients");
    private final static QName _GetAllPatientsResponse_QNAME = new QName("http://patient/", "getAllPatientsResponse");
    private final static QName _GetPatient_QNAME = new QName("http://patient/", "getPatient");
    private final static QName _GetPatientResponse_QNAME = new QName("http://patient/", "getPatientResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: patient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllPatients }
     * 
     */
    public GetAllPatients createGetAllPatients() {
        return new GetAllPatients();
    }

    /**
     * Create an instance of {@link GetAllPatientsResponse }
     * 
     */
    public GetAllPatientsResponse createGetAllPatientsResponse() {
        return new GetAllPatientsResponse();
    }

    /**
     * Create an instance of {@link GetPatient }
     * 
     */
    public GetPatient createGetPatient() {
        return new GetPatient();
    }

    /**
     * Create an instance of {@link GetPatientResponse }
     * 
     */
    public GetPatientResponse createGetPatientResponse() {
        return new GetPatientResponse();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link Prescription }
     * 
     */
    public Prescription createPrescription() {
        return new Prescription();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPatients }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllPatients }{@code >}
     */
    @XmlElementDecl(namespace = "http://patient/", name = "getAllPatients")
    public JAXBElement<GetAllPatients> createGetAllPatients(GetAllPatients value) {
        return new JAXBElement<GetAllPatients>(_GetAllPatients_QNAME, GetAllPatients.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllPatientsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllPatientsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://patient/", name = "getAllPatientsResponse")
    public JAXBElement<GetAllPatientsResponse> createGetAllPatientsResponse(GetAllPatientsResponse value) {
        return new JAXBElement<GetAllPatientsResponse>(_GetAllPatientsResponse_QNAME, GetAllPatientsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatient }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPatient }{@code >}
     */
    @XmlElementDecl(namespace = "http://patient/", name = "getPatient")
    public JAXBElement<GetPatient> createGetPatient(GetPatient value) {
        return new JAXBElement<GetPatient>(_GetPatient_QNAME, GetPatient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetPatientResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://patient/", name = "getPatientResponse")
    public JAXBElement<GetPatientResponse> createGetPatientResponse(GetPatientResponse value) {
        return new JAXBElement<GetPatientResponse>(_GetPatientResponse_QNAME, GetPatientResponse.class, null, value);
    }

}
